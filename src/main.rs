/*
Lists new users on a wiki in easy copy/paste format
Copyright (C) 2020-2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//! A [Toolforge tool](https://newusers.toolforge.org/) that provides a plain text
//! list of new users created on a wiki. By default it displays the list of new users
//! from the English Wikisource, but that can be changed by adding a `?project=en.wikisource`
//! parameter.
//!
//! This list is great for plugging into other tools to run checks or get other information.

#[macro_use]
extern crate rocket;

use mwapi::{Client, Error};
use mwapi_responses::prelude::*;
use rocket::http::Status;
use rocket_healthz::Healthz;
use std::collections::HashMap;

const USER_AGENT: &str = toolforge::user_agent!("newusers");

#[query(
    list = "logevents",
    leaction = "newusers/create",
    leprop = "user",
    lelimit = "100"
)]
struct Response;

/// Route for index requests
#[get("/?<project>")]
async fn index(project: Option<String>) -> Result<String, (Status, String)> {
    build_index(project)
        .await
        .map_err(|err| (Status::InternalServerError, format!("Error: {}", err)))
}

async fn build_index(project: Option<String>) -> Result<String, Error> {
    let api = Client::builder(&format!(
        "https://{}.org/w/api.php",
        project.unwrap_or_else(|| "en.wikisource".into())
    ))
    .set_user_agent(USER_AGENT)
    .build()
    .await?;

    let resp: Response =
        mwapi_responses::query_api(&api, HashMap::<String, String>::new())
            .await?;
    let users: Vec<_> = resp.into_items().map(|item| item.user).collect();

    Ok(users.join("\n"))
}

/// Start Rocket
#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", routes![index])
        .attach(Healthz::fairing())
}

#[cfg(test)]
mod test {
    use super::rocket;
    use rocket::http::Status;

    #[test]
    fn test_index() {
        use rocket::local::blocking::Client;
        let client = Client::tracked(rocket()).unwrap();
        let resp = client.get("/").dispatch();
        let resp2 = client.get("/?project=en.wikipedia").dispatch();
        // Assert HTTP 200
        assert_eq!(resp.status(), Status::Ok);
        assert_eq!(resp2.status(), Status::Ok);
        let string_resp = resp.into_string().unwrap();
        let string_resp2 = resp2.into_string().unwrap();
        // Verify that en.ws and en.wp responses are different
        assert_ne!(string_resp, string_resp2);
        // Assert each response has 100 names
        let split_resp: Vec<&str> = string_resp.split('\n').collect();
        assert_eq!(split_resp.len(), 100);
        let split_resp2: Vec<&str> = string_resp2.split('\n').collect();
        assert_eq!(split_resp2.len(), 100);
    }
}
